
import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class TestSimulation extends Simulation {

	val httpProtocol = http
		.baseUrl("http://computer-database.gatling.io")
		.inferHtmlResources()
		.acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8")
		.acceptEncodingHeader("gzip, deflate")
		.acceptLanguageHeader("en-US,en;q=0.9,es;q=0.8,da;q=0.7")
		.upgradeInsecureRequestsHeader("1")
		.userAgentHeader("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36")

	val headers_0 = Map("Proxy-Connection" -> "keep-alive")

	val headers_3 = Map(
		"Origin" -> "http://computer-database.gatling.io",
		"Proxy-Connection" -> "keep-alive")



	val scn = scenario("RecordedSimulation")
		// Search
		.exec(http("visit home page")
			.get("/computers")
			.headers(headers_0))
		.pause(3)
		.exec(http("search for macbook")
			.get("/computers?f=Macbook")
			.headers(headers_0))
		.pause(1)
		.exec(http("view details")
			.get("/computers/89")
			.headers(headers_0))
		.pause(2)
		.exec(http("update details")
			.post("/computers/89")
			.headers(headers_3)
			.formParam("name", "MacBook")
			.formParam("introduced", "2006-05-16")
			.formParam("discontinued", "2016-06-12")
			.formParam("company", "2"))

	setUp(scn.inject(atOnceUsers(1))).protocols(httpProtocol)
}